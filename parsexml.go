// parsexml.go
package xmlparser

import (
	"encoding/xml"
)

type Node struct {
	XMLName xml.Name
	Data    string `xml:",chardata"`
	Nodes   []Node `xml:",any"`
}

func xmlToMap(data string) (interface{}, error) {
	v := Node{}
	err := xml.Unmarshal([]byte(data), &v)
	r := make(map[string]interface{})
	r[v.XMLName.Local] = v.nodeToMap()
	return interface{}(r), err
}

func (n *Node) nodeToMap() interface{} {
	if len(n.Nodes) == 0 {
		return n.Data
	}
	m := make(map[string]interface{})
	for _, v := range n.Nodes {
		m[v.XMLName.Local] = v.nodeToMap()
	}
	return interface{}(m)
}
